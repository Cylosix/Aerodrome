package com.cylosix.aerodrome;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AirTrafficControl extends Thread {
	
	public List<Flight> radar = new ArrayList<Flight>();
	public int totalFlights = 0;
	
	public AirTrafficControl() {
		start();
	}
	
	public void run() {
		//Create an inital flight;
		Flight f = new Flight();
		radar.add(f);
		totalFlights++;
		
		//Keep creating
		do {
			try {
				Thread.sleep((new Random().nextInt(4) + 1) * 1000);
			} catch (InterruptedException e) {
				System.out.println("Error in creating flights thread | Class AirTrafficControl");
			}
			//Limit the max number of flights
			if(radar.size() < Airport.maxPlanes) {
				f = new Flight();
				radar.add(f);
				totalFlights++;
			}
		} while(this.isAlive());
	}
}
