package com.cylosix.aerodrome;

import java.awt.BorderLayout;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Frame extends Thread {

	private JFrame f;
	private JSplitPane divider;
	private JPanel statPanel;
	private JScrollPane flightPane;
	private JTable flightTable;
	SimpleDateFormat ET, cd;
	
	public Frame() {
		ET = new SimpleDateFormat("HH:mm:ss");
		cd = new SimpleDateFormat("mm:ss");
		
		//Create a JFrame
		f = new JFrame("Java Aerodrome");
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Create a panel in which to display the sta
		statPanel = new JPanel();
		
		//Create a table
		flightTable = new JTable();
		flightTable.setFillsViewportHeight(true);
		flightTable.setDefaultRenderer(Object.class, new CellRenderer());
		start();

		//Create a scrollable contentpane
		flightPane = new JScrollPane(flightTable);
		
		divider = new JSplitPane(JSplitPane.VERTICAL_SPLIT, statPanel, flightPane);
		divider.setDividerLocation(50);
		
		//Show the frame
		f.add(divider, BorderLayout.CENTER);
		f.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		//f.setUndecorated(true);
		f.setVisible(true);
	}
	
	public void run() {
		do {
			
			//Set column headers
		    String[] cols = { "FLIGHT", "TYPE", "ORIGIN", "DESTINATION", "DELAYED", "STATUS", "AIRSTRIP", "ETA / ETD", "COUNTDOWN" };
			
		    //Create empty jagged array to store each plane's information
			int size = Airport.ATC.radar.size();
			String[][] rows = new String[size][];
			
			int i = 0;
			//Loop through all the planes
			for(Iterator<Flight> it  = Airport.ATC.radar.iterator(); it.hasNext();) {
		    	Flight f = it.next();
		    	
		    	String stripName = "TBD";
				if(f.airstrip != null) {
		    		stripName = f.airstrip.name;
		    	}
				
				String delayed = "On time";
				if(f.delayed) {
					delayed = "Delayed";
				}
					    	
		    	//Crate a row in the table with the plane's information
		    	String[] row = { f.name, f.type, f.origin, f.destination, delayed, f.state.toString(), stripName, ET.format(f.ET), cd.format(f.countdown) };
		    	rows[i] = row;
		    	i++;
		    }
			
			//Set the jagged array as the datasource for the table
			DefaultTableModel model = new DefaultTableModel(rows, cols);
			flightTable.setModel(model);
			
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				System.out.println("Error in table update thread | Class Frame");
			}
		} while(this.isAlive());
	}
}
