package com.cylosix.aerodrome;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Airport {
	
	private static Random r = new Random();

	public static String name = "GCA";
	public static AirTrafficControl ATC = new AirTrafficControl();
	public static List<Airstrip> airstrips = new ArrayList<Airstrip>();
	
	/**
	 * Change the numbers below to play around with the program.
	 * ETA's and ETD's and not editable to ensure to program runs somewhat smoothly
	 * I recommend using 1 airstrip per 4 flights, but feel free to try different values.
	 */
	public static int maxPlanes = 80; //Maximum number of planes simultaneously
	public static int numAirstrips = 20; //Number of airstrips.
	public static int crashChance = 1000; //Chance is 1 in value
	
	public static void main(String[] args) {
		new Frame();
		//Create airstrips
		for(int i  = 0; i < numAirstrips; i++) {
			airstrips.add(new Airstrip(generateAirstripName()));
		}
	}
	
	//Generate a random name;
	private static String generateAirstripName(){
		int airstripNumber = r.nextInt(98) + 1;
		char airstripChar = (char)(r.nextInt(26) + 'a');
		return Integer.toString(airstripNumber) + String.valueOf(airstripChar).toUpperCase();
	}
	
	public static Airstrip stripAvailable() {
		//Check if there is a unoccupied airstrip
		for(Iterator<Airstrip> it = airstrips.iterator(); it.hasNext();) {
			Airstrip a = it.next();
			if(a.occupied == false) {
				a.occupied = true;
				return a;
			}
		}
		return null;
	}

}
