package com.cylosix.aerodrome;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

@SuppressWarnings("serial")
public class CellRenderer extends DefaultTableCellRenderer {

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    	Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        //Get the value of the second cell in a row
        String val = table.getValueAt(row, 5).toString();
        //Change background colour depending on value
        if (val.equals("landing")) {
            cellComponent.setBackground(Color.ORANGE);
        } else if (val.equals("landed")) {
            cellComponent.setBackground(Color.GREEN);
        } else if (val.equals("crashed")) {
        	cellComponent.setBackground(Color.RED);
        } else if (val.equals("departing")) {
        	cellComponent.setBackground(Color.CYAN);
        } else if (val.equals("departed")) {
        	cellComponent.setBackground(Color.YELLOW);
        } else {
        	cellComponent.setBackground(Color.WHITE);
        }

        return cellComponent;
    }
}