package com.cylosix.aerodrome;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Flight extends Thread {
	
	public String name;
	public String type;
	public String origin;
	public String destination;
	public boolean delayed = false;
	public enum states { onhold, landing, landed, departing, departed, crashed};
	public states state;
	public Airstrip airstrip;
	public Date ET; //Estimated time of arrival and departure
	public Date countdown; //Difference between current time and the ETA/ETD
	
	private Random r = new Random();
	
	public Flight() {
		//All new flights are inbound 
		name = generateName();
		type = getRandomFlightType();
		origin = getRandomAirport();
		destination = Airport.name;
		state = states.onhold;
		
		//Get current time;
		Calendar cal = Calendar.getInstance();
		long t = cal.getTimeInMillis();
		//Add a minimum of 30 seconds and a maximum of 300 seconds to the ETA
		ET = new Date(t + ((r.nextInt(60) + 60) * 1000));
		
		start();
	}
	
	public void run() {
		do {
			//Update the countdown if the plane hasn't crashed or departed
			if(state != states.departed && state != states.crashed) {
				countdown = new Date(timeDiff(ET, new Date()));
			}
			
			//Make sure the plane only has a chance to crash in certain states
			if(state == states.onhold || state == states.landing || state == states.departing) {
				if(crashPlane()) {
					state = states.crashed;
				}
			}
			
			//Check the plane's current state
			switch(state) {
				case landing:
					landing();
					break;
				case landed:
					landed();
					break;
				case departing:
					departing();
					break;
				case departed:
				case crashed:
					removePlane();
					break;
				default:
					onhold();
			}
			
			//Delay the loop for 1 second
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				System.out.println(String.format("Flight %s error | Failed sleep", name));
			}
		} while(this.isAlive());
	}
	
	private void onhold() {
		//Try to land the plane if the ETA is less than 30 seconds
		if(timeDiff(ET, new Date()) < (31 * 1000)) {
			airstrip = Airport.stripAvailable();
			if(airstrip != null) {
				//Land the plane if there is an airstrip available.
				state = states.landing;
			} else {
				//Else delay the flight by 10 seconds
				ET = new Date(ET.getTime() + (10 * 1000));
				delayed = true;
			}
		}
	}

	private void landing() {
		//Set the plane's status to landed if the ET has elapsed
		if(timeDiff(ET, new Date()) < 1000) {
			state = states.landed;
			delayed = false;
			origin = Airport.name;
			destination = getRandomAirport();
			airstrip.occupied = false; //Free up the airstrip for the next plane
			airstrip = null;
			ET = new Date(new Date().getTime() + (10 * 1000)); //Set a new ET for departure
		}
	}
	
	private void landed() {
		//Depart the plane if the ET has elapsed
		if(timeDiff(ET, new Date()) < 1000) {
			airstrip = Airport.stripAvailable();
			if(airstrip != null) {
				//Depart the plane if an airstrip is available
				state = states.departing;
				ET = new Date(new Date().getTime() + (31*1000)); //30 seconds until the plane has taken off
			} else {
				//else delay departure by 1 second
				ET = new Date(ET.getTime() + (1 * 1000));
				delayed = true;
			}
		}
	}
	
	private void departing() {
		//Set the plane as departed after ET has elapsed
		if(timeDiff(ET, new Date()) < 1000) {
			state = states.departed;
		}
	}
	
	private void removePlane() {
		//Clear airstrip
		if(airstrip != null) {
			airstrip.occupied = false;
		}
		airstrip = null;
		
		//Show the plane for 10 seconds before removing
		try {
			sleep(10 * 1000);
		} catch (InterruptedException e) {
			System.out.println("Error removing plane | Flight class");
		}
		Airport.ATC.radar.remove(this);
	}
	
	//Chance of 1 in x for the plane to crash
	private boolean crashPlane() {
		if(new Random().nextInt(Airport.crashChance) == 1){
			return true;
		}
		return false;
	}
	
	//Calculate the difference between 2 dates in milliseconds
	public long timeDiff(Date d1, Date d2) {
		long t1 = d1.getTime();
		long t2 = d2.getTime();
		long diff = t1 - t2;
		return (diff);
	}
	
	private String getRandomFlightType() {
		String[] types = { "Freight", "Instruction", "Charter", "Short-Distance", "Long-Distance", "City", "Nonstop", "Reduced Gravity" };
		int i = r.nextInt(types.length);
		return types[i] + " Flight";
	}
	
	//For getting an origin or a destination
	private String getRandomAirport() {
		String[] airports = { "HND", "LHR", "LAX", "AMS", "PEK", "DXB", "FRA", "JFK", "CDG", "IST" };
		int i = r.nextInt(airports.length);
		return airports[i];
	}
	
	//Flight names consist of 2 characters and 4 digits
	private String generateName() {
		char first = (char)(r.nextInt(26) + 'a');
		char second = (char)(r.nextInt(26) + 'a');
		int number = r.nextInt(1000) + 8999;
		return (String.valueOf(first) + String.valueOf(second) + Integer.toString(number)).toUpperCase();
	}
	
}
